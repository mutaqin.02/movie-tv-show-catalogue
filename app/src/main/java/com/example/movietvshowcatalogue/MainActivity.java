package com.example.movietvshowcatalogue;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;

import com.example.movietvshowcatalogue.helper.ReminderReceiver;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {

    private int title = R.string.title_text_1;
    public static final String APIKEY = "03c397f4994f5e7a6fee6a5b28b93906";
    public static final String KEY_FRAGMENT = "fragment";
    public static final String KEY_MOVIES = "movies";
    private Fragment pageContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setActionBarTitle(title);

        SectionsPagerAdapter sectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager(), this);
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerAdapter);
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0);
        }

        if (savedInstanceState != null) {
            pageContent = getSupportFragmentManager().getFragment(savedInstanceState, KEY_FRAGMENT);
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_fragment, pageContent).commit();
        } else {
            int tabSelect = viewPager.getCurrentItem();
            pageContent = sectionsPagerAdapter.getItem(tabSelect);

            getSupportFragmentManager().beginTransaction().replace(R.id.frame_fragment, pageContent).commit();
        }

    }

    private void setActionBarTitle(int title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_favorite);
        menuItem.setVisible(false);
        MenuItem menuItem1 = menu.findItem(R.id.action_search_main);
        menuItem1.setVisible(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_change_settings) {
            Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(mIntent);
        } else if (item.getItemId() == R.id.action_reminder_settings) {
            Intent mIntent = new Intent(this, ReminderSettingsActivity.class);
            startActivity(mIntent);
        } else if (item.getItemId() == R.id.action_menu_favorite) {
            Intent mIntent = new Intent(this,FavoriteActivity.class);
            startActivity(mIntent);
        } else if (item.getItemId() == R.id.action_search_main) {
            Intent mIntent = new Intent(this, SearchActivity.class);
            startActivity(mIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        getSupportFragmentManager().putFragment(outState, KEY_FRAGMENT, pageContent);
        super.onSaveInstanceState(outState);
    }

}
