package com.example.movietvshowcatalogue.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MovieSearch implements Parcelable {

    private int id;
    private String catg;
    private String rating;
    private String title;
    private String relase_date;
    private String overview;
    private String poster;

    public MovieSearch(int id, String catg, String rating, String title, String relase_date, String overview, String poster) {
        this.id = id;
        this.catg = catg;
        this.rating = rating;
        this.title = title;
        this.relase_date = relase_date;
        this.overview = overview;
        this.poster = poster;
    }

    protected MovieSearch(Parcel in) {
        id = in.readInt();
        catg = in.readString();
        rating = in.readString();
        title = in.readString();
        relase_date = in.readString();
        overview = in.readString();
        poster = in.readString();
    }

    public static final Creator<MovieSearch> CREATOR = new Creator<MovieSearch>() {
        @Override
        public MovieSearch createFromParcel(Parcel in) {
            return new MovieSearch(in);
        }

        @Override
        public MovieSearch[] newArray(int size) {
            return new MovieSearch[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCatg() {
        return catg;
    }

    public void setCatg(String catg) {
        this.catg = catg;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRelase_date() {
        return relase_date;
    }

    public void setRelase_date(String relase_date) {
        this.relase_date = relase_date;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(catg);
        dest.writeString(rating);
        dest.writeString(title);
        dest.writeString(relase_date);
        dest.writeString(overview);
        dest.writeString(poster);
    }
}
