package com.example.movietvshowcatalogue.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetTVShow {

    @SerializedName("results")
    List<TVShow> listTVShow;

    public List<TVShow> getListTVShow() {
        return listTVShow;
    }

    public void setListTVShow(List<TVShow> listTVShow) {
        this.listTVShow = listTVShow;
    }
}
