package com.example.movietvshowcatalogue.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MovieFavo implements Parcelable {

    private int id;
    private int id_movies;
    private String type;
    private String title;
    private String rating;
    private String relase_date;
    private String popularity;
    private String overview;
    private String poster;
    private String backdrop;

    public MovieFavo(int id, int id_movies, String type, String title, String rating, String relase_date, String popularity, String overview, String poster, String backdrop) {
        this.id = id;
        this.id_movies = id_movies;
        this.type = type;
        this.title = title;
        this.rating = rating;
        this.relase_date = relase_date;
        this.popularity = popularity;
        this.overview = overview;
        this.poster = poster;
        this.backdrop = backdrop;
    }

    protected MovieFavo(Parcel in) {
        id = in.readInt();
        id_movies = in.readInt();
        type = in.readString();
        title = in.readString();
        rating = in.readString();
        relase_date = in.readString();
        popularity = in.readString();
        overview = in.readString();
        poster = in.readString();
        backdrop = in.readString();
    }

    public static final Creator<MovieFavo> CREATOR = new Creator<MovieFavo>() {
        @Override
        public MovieFavo createFromParcel(Parcel in) {
            return new MovieFavo(in);
        }

        @Override
        public MovieFavo[] newArray(int size) {
            return new MovieFavo[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_movies() {
        return id_movies;
    }

    public void setId_movies(int id_movies) {
        this.id_movies = id_movies;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRelase_date() {
        return relase_date;
    }

    public void setRelase_date(String relase_date) {
        this.relase_date = relase_date;
    }

    public String getPopularity() {
        return popularity;
    }

    public void setPopularity(String popularity) {
        this.popularity = popularity;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getPoster() {
        return poster;
    }

    public void setPoster(String poster) {
        this.poster = poster;
    }

    public String getBackdrop() {
        return backdrop;
    }

    public void setBackdrop(String backdrop) {
        this.backdrop = backdrop;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(id_movies);
        dest.writeString(type);
        dest.writeString(title);
        dest.writeString(rating);
        dest.writeString(relase_date);
        dest.writeString(popularity);
        dest.writeString(overview);
        dest.writeString(poster);
        dest.writeString(backdrop);
    }
}
