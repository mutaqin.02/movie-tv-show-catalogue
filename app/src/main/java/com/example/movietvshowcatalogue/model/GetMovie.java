package com.example.movietvshowcatalogue.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GetMovie {

    @SerializedName("results")
    List<Movie> listMovie;

    public List<Movie> getListMovie() {
        return listMovie;
    }

    public void setListMovie(List<Movie> listMovie) {
        this.listMovie = listMovie;
    }
}
