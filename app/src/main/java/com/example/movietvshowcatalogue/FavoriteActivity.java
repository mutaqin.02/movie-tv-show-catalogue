package com.example.movietvshowcatalogue;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.material.tabs.TabLayout;

public class FavoriteActivity extends AppCompatActivity {

    private int title = R.string.title_text_favo;
    public static final String KEY_FRAGMENT_FAVO = "fragment_favo";
    public static final String KEY_MOVIES_FAVO = "movies_favo";
    private Fragment pageContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        setActionBarTitle(title);

        SectionsPagerFavoAdapter sectionsPagerFavoAdapter = new SectionsPagerFavoAdapter(getSupportFragmentManager(),this);
        ViewPager viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(sectionsPagerFavoAdapter);
        TabLayout tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setElevation(0);
        }

        if (savedInstanceState != null) {
            pageContent = getSupportFragmentManager().getFragment(savedInstanceState, KEY_FRAGMENT_FAVO);
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_fragment, pageContent).commit();
        } else {
            int tabSelect = viewPager.getCurrentItem();
            pageContent = sectionsPagerFavoAdapter.getItem(tabSelect);
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_fragment, pageContent).commit();
        }
    }

    private void setActionBarTitle(int title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        MenuItem menuItem = menu.findItem(R.id.action_favorite);
        menuItem.setVisible(false);
        MenuItem menuItem2 = menu.findItem(R.id.action_menu_favorite);
        menuItem2.setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_change_settings) {
            Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(mIntent);
        } else if (item.getItemId() == R.id.action_reminder_settings) {
            Intent mIntent = new Intent(this, ReminderSettingsActivity.class);
            startActivity(mIntent);
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        getSupportFragmentManager().putFragment(outState, KEY_FRAGMENT_FAVO, pageContent);
        super.onSaveInstanceState(outState);
    }
}
