package com.example.movietvshowcatalogue.database;

import android.provider.BaseColumns;

public class DatabaseContract {

    public static final class FavoriteColumn implements BaseColumns {
        public static final String TABLE_NAME = "favorite";
        public static final String ID_MOVIES = "id_movies";
        public static final String TYPE = "type";
        public static final String TITLE = "title";
        public static final String RATING = "rating";
        public static final String RELEASE_DATE = "release_date";
        public static final String POPULARITY = "popularity";
        public static final String OVERVIEW = "overview";
        public static final String POSTER = "poster";
        public static final String BACKDROP = "backdrop";
    }
}
