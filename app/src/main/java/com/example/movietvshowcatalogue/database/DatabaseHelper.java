package com.example.movietvshowcatalogue.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static String DATABASE_NAME = "movie_tvshow_favorite";

    private static final int DATABASE_VERSION = 1;

    private static final String SQL_CREATE_TABLE_FAVO = String.format("CREATE TABLE %s"
    +" (%s INTEGER PRIMARY KEY AUTOINCREMENT,"+
    " %s TEXT NOT NULL,"+
    " %s TEXT NOT NULL,"+
    " %s TEXT NOT NULL,"+
    " %s TEXT NOT NULL,"+
    " %s TEXT NOT NULL,"+
    " %s TEXT NOT NULL,"+
    " %s TEXT NOT NULL,"+
    " %s TEXT NOT NULL,"+
    " %s TEXT NOT NULL)",
            DatabaseContract.FavoriteColumn.TABLE_NAME,
            DatabaseContract.FavoriteColumn._ID,
            DatabaseContract.FavoriteColumn.ID_MOVIES,
            DatabaseContract.FavoriteColumn.TYPE,
            DatabaseContract.FavoriteColumn.TITLE,
            DatabaseContract.FavoriteColumn.RATING,
            DatabaseContract.FavoriteColumn.RELEASE_DATE,
            DatabaseContract.FavoriteColumn.POPULARITY,
            DatabaseContract.FavoriteColumn.OVERVIEW,
            DatabaseContract.FavoriteColumn.POSTER,
            DatabaseContract.FavoriteColumn.BACKDROP
    );

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_TABLE_FAVO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+DatabaseContract.FavoriteColumn.TABLE_NAME);
        onCreate(db);
    }
}
