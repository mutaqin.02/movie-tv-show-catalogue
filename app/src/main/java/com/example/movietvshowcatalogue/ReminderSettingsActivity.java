package com.example.movietvshowcatalogue;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.example.movietvshowcatalogue.Rest.ApiClient;
import com.example.movietvshowcatalogue.Rest.ApiService;
import com.example.movietvshowcatalogue.helper.ReminderReceiver;
import com.example.movietvshowcatalogue.model.GetMovie;
import com.example.movietvshowcatalogue.model.Movie;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReminderSettingsActivity extends AppCompatActivity {

    private Switch aSwitch_Daily;
    private Switch aSwitch_Release;
    private ReminderReceiver reminderReceiver;
    private List<Movie> movieList;
    private static final String PREFS_NAME = "reminder_pref";
    private static final String DAILY_REMIND = "daily_remind";
    private static final String RELEASE_REMIND = "release_remind";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder_settings);
        String title = getResources().getString(R.string.reminder_settings);
        setActionBarTitle(title);

        reminderReceiver = new ReminderReceiver();

        aSwitch_Daily = findViewById(R.id.dailySwitch);
        aSwitch_Release = findViewById(R.id.releaseSwitch);

        switchDaily(this);
        switchRelease(this);

        SharedPreferences prefs = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        boolean daily_selected = prefs.getBoolean(DAILY_REMIND, false);
        if (daily_selected) {
            aSwitch_Daily.setChecked(true);
        } else {
            aSwitch_Daily.setChecked(false);
        }

        boolean release_selected = prefs.getBoolean(RELEASE_REMIND, false);
        if (release_selected) {
            aSwitch_Release.setChecked(true);
        } else {
            aSwitch_Release.setChecked(false);
        }

    }

    private void setActionBarTitle(String title) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void switchDaily(final Context context) {
        aSwitch_Daily.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    String message = getResources().getString(R.string.text_msg_daily);
                    reminderReceiver.setDailyReminder(context, ReminderReceiver.TYPE_DAILY, message);
                } else {
                    reminderReceiver.cancelReminder(context, ReminderReceiver.TYPE_DAILY);
                }

                SharedPreferences pref = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(DAILY_REMIND, isChecked);
                editor.apply();
            }
        });
    }

    private void switchRelease(final Context context) {
        aSwitch_Release.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    discoverReleaseMovie(context);
                } else {
                    reminderReceiver.cancelReminder(context, ReminderReceiver.TYPE_RELEASE_TODAY);
                }

                SharedPreferences pref = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putBoolean(RELEASE_REMIND, isChecked);
                editor.apply();
            }
        });
    }

    private void discoverReleaseMovie(final Context context) {
        Date c = Calendar.getInstance().getTime();

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String dateToday = df.format(c);

        ApiService apiService = ApiClient.getService();
        Call<GetMovie> call = apiService.movieRelease(MainActivity.APIKEY, dateToday, dateToday);
        call.enqueue(new Callback<GetMovie>() {
            @Override
            public void onResponse(Call<GetMovie> call, Response<GetMovie> response) {
                if (response.body() != null) {
                    movieList = response.body().getListMovie();
                    if (movieList.size() > 0) {
                        for (int i = 0; i < movieList.size(); i++) {
                            Integer id_movie = movieList.get(i).getId();
                            String title = movieList.get(i).getTitle();
                            String message = title+" release today";
                            reminderReceiver.setReleaseTodayReminder(context,ReminderReceiver.TYPE_RELEASE_TODAY, id_movie, title, message);
                        }
                        Toast.makeText(context,getResources().getString(R.string.alert_set_release), Toast.LENGTH_LONG).show();
                    } else {
                        aSwitch_Release.setChecked(false);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetMovie> call, Throwable t) {
                aSwitch_Release.setChecked(false);
            }
        });
    }
}
