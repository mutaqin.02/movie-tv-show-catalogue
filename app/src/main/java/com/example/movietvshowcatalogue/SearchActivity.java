package com.example.movietvshowcatalogue;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.SearchManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.example.movietvshowcatalogue.Rest.ApiClient;
import com.example.movietvshowcatalogue.Rest.ApiService;
import com.example.movietvshowcatalogue.adapter.CardViewSearchAdapter;
import com.example.movietvshowcatalogue.model.GetMovie;
import com.example.movietvshowcatalogue.model.GetTVShow;
import com.example.movietvshowcatalogue.model.Movie;
import com.example.movietvshowcatalogue.model.MovieSearch;
import com.example.movietvshowcatalogue.model.TVShow;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private List<Movie> movieList;
    private List<TVShow> tvShowsList;
    private ArrayList<MovieSearch> searchList = new ArrayList<>();
    private CardViewSearchAdapter cardViewSearchAdapter;
    private SearchView searchView;
    private static final String KEY_SEARCH = "movies_search";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        progressBar = findViewById(R.id.progressBar);
        recyclerView = findViewById(R.id.movie_list);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        cardViewSearchAdapter = new CardViewSearchAdapter(this);
        recyclerView.setAdapter(cardViewSearchAdapter);

        if (savedInstanceState != null) {
            ArrayList<MovieSearch> list = savedInstanceState.getParcelableArrayList(KEY_SEARCH);
            if (list != null) {
                cardViewSearchAdapter.setListMovie(list);
            }
        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(getApplicationContext().SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);

        searchView.onActionViewExpanded();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (query.length() > 3) {
                    showRecyclerCardView(query);
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.length() > 3) {
                    showRecyclerCardView(newText);
                }
                return false;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }

    private void showRecyclerCardView(String text) {
        showLoading(true);
        searchList.clear();
        SearchMovies(text);
        SearchTVShow(text);
    }

    private void SearchMovies(String text){
        ApiService apiService = ApiClient.getService();
        Call<GetMovie> call = apiService.SearchMovies(MainActivity.APIKEY, "en-US", text);
        call.enqueue(new Callback<GetMovie>() {
            @Override
            public void onResponse(Call<GetMovie> call, Response<GetMovie> response) {
                showLoading(false);
                if (response.body() != null) {
                    movieList = response.body().getListMovie();
                    if (movieList.size() > 0) {
                        for (int i=0;i<movieList.size();i++) {
                            String catg = "Movie";
                            Integer id_movie = movieList.get(i).getId();
                            String dataTitle = movieList.get(i).getTitle();
                            String dataVote = movieList.get(i).getVote_average();
                            String dataReleaseDate = movieList.get(i).getRelease_date();
                            String dataOverview = movieList.get(i).getOverview();
                            String dataPoster = movieList.get(i).getPoster_path();
                            searchList.add(new MovieSearch(id_movie,catg,dataVote,dataTitle,dataReleaseDate,dataOverview,dataPoster));
                        }
                        cardViewSearchAdapter.setListMovie(searchList);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetMovie> call, Throwable t) {
                showLoading(false);
            }
        });
    }

    private void SearchTVShow(String text){
        ApiService apiService = ApiClient.getService();
        Call<GetTVShow> call = apiService.SearchMTVShow(MainActivity.APIKEY, "en-US", text);
        call.enqueue(new Callback<GetTVShow>() {
            @Override
            public void onResponse(Call<GetTVShow> call, Response<GetTVShow> response) {
                showLoading(false);
                if (response.body() != null) {
                    tvShowsList = response.body().getListTVShow();
                    if (tvShowsList.size() > 0) {
                        for (int i=0;i<tvShowsList.size();i++) {
                            String catg = "TVShow";
                            Integer id_movie = tvShowsList.get(i).getId();
                            String dataTitle = tvShowsList.get(i).getTitle();
                            String dataVote = tvShowsList.get(i).getVote_average();
                            String dataReleaseDate = tvShowsList.get(i).getRelease_date();
                            String dataOverview = tvShowsList.get(i).getOverview();
                            String dataPoster = tvShowsList.get(i).getPoster_path();
                            searchList.add(new MovieSearch(id_movie,catg,dataVote,dataTitle,dataReleaseDate,dataOverview,dataPoster));
                        }
                        cardViewSearchAdapter.setListMovie(searchList);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetTVShow> call, Throwable t) {
                showLoading(false);
            }
        });
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelableArrayList(KEY_SEARCH, cardViewSearchAdapter.getListMovie());
        super.onSaveInstanceState(outState);
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

}
