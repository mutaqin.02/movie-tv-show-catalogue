package com.example.movietvshowcatalogue.helper;

import android.database.Cursor;

import com.example.movietvshowcatalogue.database.DatabaseContract;
import com.example.movietvshowcatalogue.model.MovieFavo;

import java.util.ArrayList;

public class MappingHelper {

    public static ArrayList<MovieFavo> mapCursorToArrayList(Cursor noteCursor) {
        ArrayList<MovieFavo> movieFavos = new ArrayList<>();

        while (noteCursor.moveToNext()) {
            int id = noteCursor.getInt(noteCursor.getColumnIndexOrThrow(DatabaseContract.FavoriteColumn._ID));
            int id_movies = noteCursor.getInt(noteCursor.getColumnIndexOrThrow(DatabaseContract.FavoriteColumn.ID_MOVIES));
            String type = noteCursor.getString(noteCursor.getColumnIndexOrThrow(DatabaseContract.FavoriteColumn.TYPE));
            String title = noteCursor.getString(noteCursor.getColumnIndexOrThrow(DatabaseContract.FavoriteColumn.TITLE));
            String rating = noteCursor.getString(noteCursor.getColumnIndexOrThrow(DatabaseContract.FavoriteColumn.RATING));
            String dates = noteCursor.getString(noteCursor.getColumnIndexOrThrow(DatabaseContract.FavoriteColumn.RELEASE_DATE));
            String popular = noteCursor.getString(noteCursor.getColumnIndexOrThrow(DatabaseContract.FavoriteColumn.POPULARITY));
            String overview = noteCursor.getString(noteCursor.getColumnIndexOrThrow(DatabaseContract.FavoriteColumn.OVERVIEW));
            String poster = noteCursor.getString(noteCursor.getColumnIndexOrThrow(DatabaseContract.FavoriteColumn.POSTER));
            String backdrop = noteCursor.getString(noteCursor.getColumnIndexOrThrow(DatabaseContract.FavoriteColumn.BACKDROP));
            movieFavos.add(new MovieFavo(id, id_movies, type,title,rating,dates,popular,overview,poster,backdrop));
        }

        return movieFavos;
    }
}
