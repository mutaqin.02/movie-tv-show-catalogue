package com.example.movietvshowcatalogue.helper;

public class CustomDate {

    public String getMonth(int month) {
        String stringMonth = "";

        if (month == 0) {
            stringMonth = "January";
        } else if (month == 1) {
            stringMonth = "February";
        } else if (month == 2) {
            stringMonth = "March";
        } else if (month == 3) {
            stringMonth = "April";
        } else if (month == 4) {
            stringMonth = "May";
        } else if (month == 5) {
            stringMonth = "June";
        } else if (month == 6) {
            stringMonth = "July";
        } else if (month == 7) {
            stringMonth = "August";
        } else if (month == 8) {
            stringMonth = "September";
        } else if (month == 9) {
            stringMonth = "October";
        } else if (month == 10) {
            stringMonth = "November";
        } else if (month == 11) {
            stringMonth = "December";
        }

        return stringMonth;
    }

    public String getDay(int day) {
        String stringDay = "";

        switch (day) {
            case 1:
                stringDay = "Sunday";
                break;
            case 2:
                stringDay = "Monday";
                break;
            case 3:
                stringDay = "Tuesday";
                break;
            case 4:
                stringDay = "Wednesday";
                break;
            case 5:
                stringDay = "Thursday";
                break;
            case 6:
                stringDay = "Friday";
                break;
            case 7:
                stringDay = "Saturday";
                break;
                default:
                    stringDay = "-";
                    break;
        }
        return stringDay;
    }

}
