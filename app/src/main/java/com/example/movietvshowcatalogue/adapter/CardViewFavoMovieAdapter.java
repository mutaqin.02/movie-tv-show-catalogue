package com.example.movietvshowcatalogue.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.movietvshowcatalogue.MovieTvShowDetails;
import com.example.movietvshowcatalogue.R;
import com.example.movietvshowcatalogue.helper.CustomDate;
import com.example.movietvshowcatalogue.model.MovieFavo;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CardViewFavoMovieAdapter extends RecyclerView.Adapter<CardViewFavoMovieAdapter.FavoMovieViewHolder> {

    private ArrayList<MovieFavo> favoArrayList = new ArrayList<>();
    private Context mContext;

    public CardViewFavoMovieAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public ArrayList<MovieFavo> getFavoArrayList() {
        return favoArrayList;
    }

    public void setFavoArrayList(ArrayList<MovieFavo> favoArrayList) {
        this.favoArrayList.clear();
        this.favoArrayList.addAll(favoArrayList);

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public FavoMovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_movie, parent, false);
        return new FavoMovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull FavoMovieViewHolder holder, int position) {
        final MovieFavo movieFavo = favoArrayList.get(position);

        CustomDate customDate = new CustomDate();

        holder.tvRate.setText(movieFavo.getRating());
        holder.tvName.setText(movieFavo.getTitle());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date1 = simpleDateFormat.parse(movieFavo.getRelase_date());
            Calendar cal = Calendar.getInstance();
            cal.setTime(date1);
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);
            int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

            String dayMonth = "";
            if (day < 10) {
                dayMonth = "0" + day;
            } else {
                dayMonth = String.valueOf(day);
            }

            String cekDay = customDate.getDay(dayOfWeek);
            String dayWeek = cekDay.substring(0, 3);

            String cekMonth = customDate.getMonth(month);

            String tglRilis = dayWeek + ", " + dayMonth + " " + cekMonth + " " + year;

            holder.tvTglRilis.setText(tglRilis);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.tvDesc.setText(movieFavo.getOverview());

        String poster = "https://image.tmdb.org/t/p/w154" + movieFavo.getPoster();

        Glide.with(holder.itemView.getContext())
                .load(poster)
                .apply(new RequestOptions().override(150, 225))
                .into(holder.imgPhoto);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MovieTvShowDetails.class);
                intent.putExtra(MovieTvShowDetails.MOVIES_TVSHOW_TITLE, "Movie");
                intent.putExtra(MovieTvShowDetails.MOVIES_TVSHOW_FAVO, "Movie");
                intent.putExtra(MovieTvShowDetails.MOVIES_TVSHOW_ID, movieFavo.getId_movies());
                mContext.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return favoArrayList.size();
    }

    public class FavoMovieViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvTglRilis;
        TextView tvDesc ;
        TextView tvRate;
        ImageView imgPhoto;

        public FavoMovieViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_item_name);
            tvTglRilis = itemView.findViewById(R.id.tv_item_tglrilis);
            tvDesc = itemView.findViewById(R.id.tv_item_desc);
            imgPhoto = itemView.findViewById(R.id.img_item_movie);
            tvRate = itemView.findViewById(R.id.tv_item_rate);
        }
    }
}
