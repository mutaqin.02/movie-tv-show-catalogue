package com.example.movietvshowcatalogue.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.movietvshowcatalogue.MovieTvShowDetails;
import com.example.movietvshowcatalogue.R;
import com.example.movietvshowcatalogue.helper.CustomDate;
import com.example.movietvshowcatalogue.model.MovieSearch;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CardViewSearchAdapter extends RecyclerView.Adapter<CardViewSearchAdapter.CardViewHolder> {

    private ArrayList<MovieSearch> listMovie = new ArrayList<>();
    private Context mContext;

    public CardViewSearchAdapter(Context mContext) {
        this.mContext = mContext;
    }

    public ArrayList<MovieSearch> getListMovie() {
        return listMovie;
    }

    public void setListMovie(ArrayList<MovieSearch> listMovie) {
        this.listMovie.clear();
        this.listMovie.addAll(listMovie);

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_search, parent, false);
        return new CardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewHolder holder, int position) {
        CustomDate customDate = new CustomDate();
        final MovieSearch movieSearch = listMovie.get(position);

        holder.tvCatg.setText(movieSearch.getCatg());
        holder.tvRate.setText(movieSearch.getRating());
        holder.tvName.setText(movieSearch.getTitle());

        if (movieSearch.getRelase_date() != null && !movieSearch.getRelase_date().isEmpty()) {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date1 = simpleDateFormat.parse(movieSearch.getRelase_date());
                Calendar cal = Calendar.getInstance();
                cal.setTime(date1);
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

                String dayMonth = "";
                if (day < 10) {
                    dayMonth = "0" + day;
                } else {
                    dayMonth = String.valueOf(day);
                }

                String cekDay = customDate.getDay(dayOfWeek);
                String dayWeek = cekDay.substring(0, 3);

                String cekMonth = customDate.getMonth(month);

                String tglRilis = dayWeek + ", " + dayMonth + " " + cekMonth + " " + year;

                holder.tvTglRilis.setText(tglRilis);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else {
            holder.tvTglRilis.setText("-");
        }

        holder.tvDesc.setText(movieSearch.getOverview());

        if (movieSearch.getPoster() != null && !movieSearch.getPoster().isEmpty()) {
            holder.imageText.setVisibility(View.INVISIBLE);
            String poster = "https://image.tmdb.org/t/p/w154" + movieSearch.getPoster();

            Glide.with(holder.itemView.getContext())
                    .load(poster)
                    .apply(new RequestOptions().override(150, 225))
                    .into(holder.imgPhoto);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MovieTvShowDetails.class);
                intent.putExtra(MovieTvShowDetails.MOVIES_TVSHOW_TITLE, movieSearch.getCatg());
                intent.putExtra(MovieTvShowDetails.MOVIES_TVSHOW_ID, movieSearch.getId());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listMovie.size();
    }

    public class CardViewHolder extends RecyclerView.ViewHolder {
        TextView tvCatg;
        TextView tvName;
        TextView tvTglRilis;
        TextView tvDesc ;
        TextView tvRate;
        ImageView imgPhoto;
        TextView imageText;

        public CardViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCatg = itemView.findViewById(R.id.label_catg);
            tvName = itemView.findViewById(R.id.tv_item_name);
            tvTglRilis = itemView.findViewById(R.id.tv_item_tglrilis);
            tvDesc = itemView.findViewById(R.id.tv_item_desc);
            imgPhoto = itemView.findViewById(R.id.img_item_movie);
            imageText = itemView.findViewById(R.id.myImageViewText);
            tvRate = itemView.findViewById(R.id.tv_item_rate);
        }
    }
}
