package com.example.movietvshowcatalogue.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.movietvshowcatalogue.MovieTvShowDetails;
import com.example.movietvshowcatalogue.R;
import com.example.movietvshowcatalogue.helper.CustomDate;
import com.example.movietvshowcatalogue.model.TVShow;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CardViewTVShowAdapter extends RecyclerView.Adapter<CardViewTVShowAdapter.CardViewViewHolder> {
    private List<TVShow> tvShowList;
    private Context mContext;
    private CustomDate customDate;

    public CardViewTVShowAdapter(List<TVShow> tvShowList, Context mContext) {
        this.tvShowList = tvShowList;
        this.mContext = mContext;
    }

    public void refill(List<TVShow> items) {
        this.tvShowList = items;

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public CardViewViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_card_movie, parent, false);
        return new CardViewViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CardViewViewHolder holder, int position) {
        final TVShow tvShow = tvShowList.get(position);

        customDate = new CustomDate();

        holder.tvRate.setText(tvShow.getVote_average());
        holder.tvName.setText(tvShow.getTitle());

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date1 = simpleDateFormat.parse(tvShow.getRelease_date());
            Calendar cal = Calendar.getInstance();
            cal.setTime(date1);
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);
            int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

            String dayMonth = "";
            if (day < 10) {
                dayMonth = "0" + day;
            } else {
                dayMonth = String.valueOf(day);
            }

            String cekDay = customDate.getDay(dayOfWeek);
            String dayWeek = cekDay.substring(0, 3);

            String cekMonth = customDate.getMonth(month);

            String tglRilis = dayWeek + ", " + dayMonth + " " + cekMonth + " " + year;

            holder.tvTglRilis.setText(tglRilis);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        holder.tvDesc.setText(tvShow.getOverview());

        String poster = "https://image.tmdb.org/t/p/w154" + tvShow.getPoster_path();

        Glide.with(holder.itemView.getContext())
                .load(poster)
                .apply(new RequestOptions().override(150, 225))
                .into(holder.imgPhoto);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, MovieTvShowDetails.class);
                intent.putExtra(MovieTvShowDetails.MOVIES_TVSHOW_TITLE, "TVShow");
                intent.putExtra(MovieTvShowDetails.MOVIES_TVSHOW_ID, tvShow.getId());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return tvShowList.size();
    }

    public List<TVShow> getList() {
        return tvShowList;
    }

    public class CardViewViewHolder extends RecyclerView.ViewHolder {
        TextView tvName;
        TextView tvTglRilis;
        TextView tvDesc ;
        TextView tvRate;
        ImageView imgPhoto;

        public CardViewViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.tv_item_name);
            tvTglRilis = itemView.findViewById(R.id.tv_item_tglrilis);
            tvDesc = itemView.findViewById(R.id.tv_item_desc);
            imgPhoto = itemView.findViewById(R.id.img_item_movie);
            tvRate = itemView.findViewById(R.id.tv_item_rate);
        }
    }
}
