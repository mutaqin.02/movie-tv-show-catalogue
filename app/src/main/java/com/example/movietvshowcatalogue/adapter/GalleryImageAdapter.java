package com.example.movietvshowcatalogue.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

public class GalleryImageAdapter extends BaseAdapter {

    private Context mContext;
    public String[] mmImage;

    public GalleryImageAdapter(Context mContext, String[] mmImage) {
        this.mContext = mContext;
        this.mmImage = mmImage;
    }

    @Override
    public int getCount() {
        return mmImage.length;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ImageView i = new ImageView(mContext);

        URL url;
        try {
            url = new URL(mmImage[position]);
            URLConnection urlConnection = url.openConnection();
            urlConnection.connect();
            InputStream inputStream = urlConnection.getInputStream();
            BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
            Bitmap bitmap = BitmapFactory.decodeStream(bufferedInputStream);
            i.setImageBitmap(bitmap);
            i.setScaleType(ImageView.ScaleType.FIT_CENTER);
            i.setLayoutParams(new Gallery.LayoutParams(150,225));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return i;
    }
}
