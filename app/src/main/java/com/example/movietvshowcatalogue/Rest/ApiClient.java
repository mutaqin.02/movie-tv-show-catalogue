package com.example.movietvshowcatalogue.Rest;

import com.example.movietvshowcatalogue.BuildConfig;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static Retrofit retrofit = null;
    protected static String URL = "https://api.themoviedb.org/3/";
    private static OkHttpClient client;

    private static Retrofit getClient(String url) {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        if (BuildConfig.DEBUG) {
            client = new OkHttpClient.Builder()
                    .connectTimeout(220, TimeUnit.SECONDS)
                    .writeTimeout(220,TimeUnit.SECONDS)
                    .readTimeout(220,TimeUnit.SECONDS)
                    .addInterceptor(loggingInterceptor)
                    .build();
        } else {
            client = new OkHttpClient.Builder()
                    .connectTimeout(220, TimeUnit.SECONDS)
                    .writeTimeout(220, TimeUnit.SECONDS)
                    .readTimeout(220, TimeUnit.SECONDS)
                    .build();
        }

        if (retrofit == null) {
            Retrofit.Builder builder = new Retrofit.Builder()
                    .baseUrl(url)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client);
            retrofit = builder.build();
        }

        return retrofit;
    }

    public static ApiService getService() {
        ApiService apiService = getClient(URL).create(ApiService.class);
        return apiService;
    }
}
