package com.example.movietvshowcatalogue.Rest;

import com.example.movietvshowcatalogue.model.GetMovie;
import com.example.movietvshowcatalogue.model.GetTVShow;
import com.example.movietvshowcatalogue.model.MovieDetail;
import com.example.movietvshowcatalogue.model.TVShowDetail;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiService {

    @GET("discover/movie")
    Call<GetMovie> movieDiscover(@Query("api_key") String api_key,
                                 @Query("language") String language,
                                 @Query("sort_by") String sort_by,
                                 @Query("page") String page);

    @GET("discover/tv")
    Call<GetTVShow> TVShowDiscover(@Query("api_key") String api_key,
                                   @Query("language") String language,
                                   @Query("sort_by") String sort_by,
                                   @Query("page") String page);

    @GET("movie/{id}")
    Call<MovieDetail> movieDetail(@Path("id") int id,
                                  @Query("api_key") String api_key,
                                  @Query("language") String language);

    @GET("tv/{id}")
    Call<TVShowDetail> TVShowDetail(@Path("id") int id,
                                    @Query("api_key") String api_key,
                                    @Query("language") String language);

    @GET("search/movie")
    Call<GetMovie> SearchMovies(@Query("api_key") String api_key,
                                    @Query("language") String language,
                                    @Query("query") String textSearch);

    @GET("search/tv")
    Call<GetTVShow> SearchMTVShow(@Query("api_key") String api_key,
                                    @Query("language") String language,
                                    @Query("query") String textSearch);

    @GET("discover/movie")
    Call<GetMovie> movieRelease(@Query("api_key") String api_key,
                                 @Query("primary_release_date.gte") String date_gte,
                                 @Query("primary_release_date.lte") String date_lte);
}
