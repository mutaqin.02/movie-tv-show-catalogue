package com.example.movietvshowcatalogue.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.movietvshowcatalogue.MainActivity;
import com.example.movietvshowcatalogue.R;
import com.example.movietvshowcatalogue.Rest.ApiClient;
import com.example.movietvshowcatalogue.Rest.ApiService;
import com.example.movietvshowcatalogue.adapter.CardViewTVShowAdapter;
import com.example.movietvshowcatalogue.model.GetTVShow;
import com.example.movietvshowcatalogue.model.TVShow;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class TVShowFragment extends Fragment {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private List<TVShow> tvShowList;
    private CardViewTVShowAdapter cardViewTVShowAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<TVShow> listTVShow = new ArrayList<>();

    public TVShowFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tvshow, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshLayout = view.findViewById(R.id.swLayout);
        progressBar = view.findViewById(R.id.progressBar);

        recyclerView = view.findViewById(R.id.tvshow_list);
        recyclerView.setHasFixedSize(true);
        if (savedInstanceState != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            cardViewTVShowAdapter = new CardViewTVShowAdapter(tvShowList, getContext());
            recyclerView.setAdapter(cardViewTVShowAdapter);
            listTVShow = savedInstanceState.getParcelableArrayList(MainActivity.KEY_MOVIES);
            cardViewTVShowAdapter.refill(listTVShow);
        } else {
            showLoading(true);
            showRecyclerCardView();
        }
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showRecyclerCardView();
            }
        });

    }

    private void showRecyclerCardView() {
        ApiService apiService = ApiClient.getService();
        Call<GetTVShow> call = apiService.TVShowDiscover(MainActivity.APIKEY, "en-US", "popularity.desc", "1");
        call.enqueue(new Callback<GetTVShow>() {
            @Override
            public void onResponse(Call<GetTVShow> call, Response<GetTVShow> response) {
                showLoading(false);
                swipeRefreshLayout.setRefreshing(false);
                if (response.body() == null) {
                    Toast.makeText(getContext(), getResources().getString(R.string.failLoadTVShow), Toast.LENGTH_LONG).show();
                } else {
                    tvShowList = response.body().getListTVShow();
                    if (tvShowList.size() == 0) {
                        Toast.makeText(getContext(), getResources().getString(R.string.failLoadTVShow), Toast.LENGTH_LONG).show();
                    } else {
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        cardViewTVShowAdapter = new CardViewTVShowAdapter(tvShowList, getContext());
                        recyclerView.setAdapter(cardViewTVShowAdapter);
                        cardViewTVShowAdapter.refill(tvShowList);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetTVShow> call, Throwable t) {
                Toast.makeText(getContext(), getResources().getString(R.string.failLoadTVShow), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        if (cardViewTVShowAdapter.getList() != null) {
            outState.putParcelableArrayList(MainActivity.KEY_MOVIES, new ArrayList<TVShow>(cardViewTVShowAdapter.getList()));
        }
        super.onSaveInstanceState(outState);
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

}
