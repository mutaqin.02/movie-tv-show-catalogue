package com.example.movietvshowcatalogue.fragments;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.movietvshowcatalogue.MainActivity;
import com.example.movietvshowcatalogue.R;
import com.example.movietvshowcatalogue.Rest.ApiClient;
import com.example.movietvshowcatalogue.Rest.ApiService;
import com.example.movietvshowcatalogue.adapter.CardViewMovieAdapter;
import com.example.movietvshowcatalogue.model.GetMovie;
import com.example.movietvshowcatalogue.model.Movie;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class MoviesFragment extends Fragment {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private List<Movie> movieList;
    private CardViewMovieAdapter cardViewMovieAdapter;
    private SwipeRefreshLayout swipeRefreshLayout;
    private ArrayList<Movie> listMovie = new ArrayList<>();

    public MoviesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_movies, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        swipeRefreshLayout = view.findViewById(R.id.swLayout);
        progressBar = view.findViewById(R.id.progressBar);

        recyclerView = view.findViewById(R.id.movie_list);
        recyclerView.setHasFixedSize(true);
        if (savedInstanceState != null) {
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            cardViewMovieAdapter = new CardViewMovieAdapter(movieList, getContext());
            recyclerView.setAdapter(cardViewMovieAdapter);
            listMovie = savedInstanceState.getParcelableArrayList(MainActivity.KEY_MOVIES);
            cardViewMovieAdapter.refill(listMovie);
        } else {
            showLoading(true);
            showRecyclerCardView();
        }
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                showRecyclerCardView();
            }
        });
    }

    private void showRecyclerCardView() {
        ApiService apiService = ApiClient.getService();
        Call<GetMovie> call = apiService.movieDiscover(MainActivity.APIKEY, "en-US", "popularity.desc", "1");
        call.enqueue(new Callback<GetMovie>() {
            @Override
            public void onResponse(Call<GetMovie> call, Response<GetMovie> response) {
                showLoading(false);
                swipeRefreshLayout.setRefreshing(false);
                if (response.body() == null) {
                    Toast.makeText(getContext(), getResources().getString(R.string.failLoadMovie), Toast.LENGTH_LONG).show();
                } else {
                    movieList = response.body().getListMovie();
                    if (movieList.size() == 0) {
                        Toast.makeText(getContext(), getResources().getString(R.string.failLoadMovie), Toast.LENGTH_LONG).show();
                    } else {
                        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
                        cardViewMovieAdapter = new CardViewMovieAdapter(movieList, getContext());
                        recyclerView.setAdapter(cardViewMovieAdapter);
                        cardViewMovieAdapter.refill(movieList);
                    }
                }
            }

            @Override
            public void onFailure(Call<GetMovie> call, Throwable t) {
                showLoading(false);
                Toast.makeText(getContext(), getResources().getString(R.string.failLoadMovie), Toast.LENGTH_LONG).show();
            }
        });

    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        if (cardViewMovieAdapter.getList() != null) {
            outState.putParcelableArrayList(MainActivity.KEY_MOVIES, new ArrayList<Movie>(cardViewMovieAdapter.getList()));
        }
        super.onSaveInstanceState(outState);
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

}
