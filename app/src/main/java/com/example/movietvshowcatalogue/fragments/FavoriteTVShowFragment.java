package com.example.movietvshowcatalogue.fragments;


import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.movietvshowcatalogue.FavoriteActivity;
import com.example.movietvshowcatalogue.R;
import com.example.movietvshowcatalogue.adapter.CardViewFavoMovieAdapter;
import com.example.movietvshowcatalogue.database.FavoriteHelper;
import com.example.movietvshowcatalogue.helper.MappingHelper;
import com.example.movietvshowcatalogue.model.MovieFavo;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteTVShowFragment extends Fragment implements LoadFavoCallback {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private CardViewFavoMovieAdapter cardViewFavoMovieAdapter;
    private FavoriteHelper favoriteHelper;
    private TextView empty_tvshow;

    public FavoriteTVShowFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite_tvshow, container, false);
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        favoriteHelper = FavoriteHelper.getInstance(getContext());
        favoriteHelper.open();
        progressBar = view.findViewById(R.id.progressBar);
        empty_tvshow = view.findViewById(R.id.empty_tvshow);

        recyclerView = view.findViewById(R.id.tvshow_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setHasFixedSize(true);
        cardViewFavoMovieAdapter = new CardViewFavoMovieAdapter(getContext());
        recyclerView.setAdapter(cardViewFavoMovieAdapter);

        if (savedInstanceState != null) {
            ArrayList<MovieFavo> list = savedInstanceState.getParcelableArrayList(FavoriteActivity.KEY_MOVIES_FAVO);
            if (list != null) {
                cardViewFavoMovieAdapter.setFavoArrayList(list);
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        new LoadFavoMovieAsync(favoriteHelper, this).execute();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putParcelableArrayList(FavoriteActivity.KEY_MOVIES_FAVO, cardViewFavoMovieAdapter.getFavoArrayList());
        super.onSaveInstanceState(outState);
    }

    @Override
    public void preExecute() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showLoading(true);
            }
        });
    }

    @Override
    public void postExecute(ArrayList<MovieFavo> favos) {
        showLoading(false);
        if (favos.size() > 0) {
            cardViewFavoMovieAdapter.setFavoArrayList(favos);
        } else {
            cardViewFavoMovieAdapter.setFavoArrayList(new ArrayList<MovieFavo>());
            empty_tvshow.setVisibility(View.VISIBLE);
        }
    }

    private static class LoadFavoMovieAsync extends AsyncTask<Void, Void, ArrayList<MovieFavo>> {

        private final WeakReference<FavoriteHelper> weakFavoHelper;
        private final WeakReference<LoadFavoCallback> weakCallback;

        private LoadFavoMovieAsync(FavoriteHelper favoriteHelper, LoadFavoCallback callback) {
            weakFavoHelper = new WeakReference<>(favoriteHelper);
            weakCallback = new WeakReference<>(callback);
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            weakCallback.get().preExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<MovieFavo> favos) {
            super.onPostExecute(favos);
            weakCallback.get().postExecute(favos);
        }

        @Override
        protected ArrayList<MovieFavo> doInBackground(Void... voids) {
            Cursor dataCursor = weakFavoHelper.get().queryByType("TVShow");
            return MappingHelper.mapCursorToArrayList(dataCursor);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        favoriteHelper.close();
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

}
