package com.example.movietvshowcatalogue;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.example.movietvshowcatalogue.database.FavoriteHelper;
import com.example.movietvshowcatalogue.helper.MappingHelper;
import com.example.movietvshowcatalogue.model.MovieFavo;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class StackRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {

    private List<MovieFavo> mWidgetItems = new ArrayList<>();
    private final Context mContext;
    private FavoriteHelper favoriteHelper;
    private Cursor cursor;

    public StackRemoteViewsFactory(Context mContext) {
        this.mContext = mContext;
    }

    @Override
    public void onCreate() {
        favoriteHelper = FavoriteHelper.getInstance(mContext);
        favoriteHelper.open();
    }

    @Override
    public void onDataSetChanged() {
        if (cursor != null) {
            cursor.close();
        }

        cursor = favoriteHelper.queryAll();
        mWidgetItems = MappingHelper.mapCursorToArrayList(cursor);
    }

    @Override
    public void onDestroy() {
        favoriteHelper.close();
    }

    @Override
    public int getCount() {
        return mWidgetItems.size();
    }

    @Override
    public RemoteViews getViewAt(int position) {
        RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.widget_item);
        int id_movies = mWidgetItems.get(position).getId_movies();
        String type_catg = mWidgetItems.get(position).getType();
        String poster = "https://image.tmdb.org/t/p/w154" + mWidgetItems.get(position).getPoster();
        try {
            URL url = new URL(poster);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream inputStream = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(inputStream);
            rv.setImageViewBitmap(R.id.imageView, myBitmap);

            Bundle extras = new Bundle();
            extras.putInt(FavoriteBannerWidget.EXTRA_ITEM, id_movies);
            extras.putString(FavoriteBannerWidget.TYPE_CATG, type_catg);
            Intent fillIntent = new Intent();
            fillIntent.putExtras(extras);

            rv.setOnClickFillInIntent(R.id.imageView, fillIntent);
            return rv;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public RemoteViews getLoadingView() {
        return null;
    }

    @Override
    public int getViewTypeCount() {
        return 1;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }
}
