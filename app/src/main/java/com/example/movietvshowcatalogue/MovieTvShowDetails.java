package com.example.movietvshowcatalogue;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.appwidget.AppWidgetManager;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.provider.Settings;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.movietvshowcatalogue.Rest.ApiClient;
import com.example.movietvshowcatalogue.Rest.ApiService;
import com.example.movietvshowcatalogue.adapter.GalleryImageAdapter;
import com.example.movietvshowcatalogue.database.DatabaseContract;
import com.example.movietvshowcatalogue.database.FavoriteHelper;
import com.example.movietvshowcatalogue.helper.CustomDate;
import com.example.movietvshowcatalogue.helper.MappingHelper;
import com.example.movietvshowcatalogue.model.MovieDetail;
import com.example.movietvshowcatalogue.model.MovieFavo;
import com.example.movietvshowcatalogue.model.TVShowDetail;

import java.lang.ref.WeakReference;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieTvShowDetails extends AppCompatActivity implements LoadFavoCallback {

    private ProgressBar progressBar;
    private CustomDate customDate;
    TextView detMovieName, detTglRilis, detPopularity, detMovieDesc, detRating;
    Gallery gallery;
    ImageView imageView;
    public static final String MOVIES_TVSHOW_TITLE = "movie_tvshow_title";
    public static final String MOVIES_TVSHOW_FAVO = "movie_tvshow_favo";
    public static final String MOVIES_TVSHOW_ID = "movie_tvshow_id";
    private ArrayList<String> lists = new ArrayList<String>();
    private ArrayList<String> dataSaved = new ArrayList<String>();
    private final String CATG_FAVO = "catg_favo";
    private final String TITLE_DETAIL = "title_detail";
    private final String ID_DETAIL = "id_detail";
    private final String SOME_VALUE_KEY = "someValueToSave";
    private String title_frag, titleDetail, catg_favo;
    private FavoriteHelper favoriteHelper;
    private Integer getId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_tv_show_details);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        link();

        catg_favo = getIntent().getStringExtra(MOVIES_TVSHOW_FAVO);

        if (savedInstanceState != null) {
            getId = savedInstanceState.getInt(ID_DETAIL);
            catg_favo = savedInstanceState.getString(CATG_FAVO);
            titleDetail = savedInstanceState.getString(TITLE_DETAIL);
            dataSaved = savedInstanceState.getStringArrayList(SOME_VALUE_KEY);
            title_frag = titleDetail;
            if (title_frag.equals("TVShow")) {
                setActionBarTitle(R.string.title_text_tvshow);
            } else {
                setActionBarTitle(R.string.title_text_movie);
            }
            lists.addAll(dataSaved);
            responDetail(dataSaved);
        } else {
            title_frag = getIntent().getStringExtra(MOVIES_TVSHOW_TITLE);

            getId = getIntent().getIntExtra(MOVIES_TVSHOW_ID, 0);
            String idMovie = getId.toString();
            new LoadFavoMovieAsync(favoriteHelper,this,idMovie).execute();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (catg_favo != null) {
            String idMovie = getId.toString();
            new LoadFavoMovieAsync(favoriteHelper,this,idMovie).execute();
        }
    }

    private void Connection(Integer getId) {
        ApiService apiService = ApiClient.getService();
        showLoading(true);
        if (title_frag.equals("TVShow")) {
            setActionBarTitle(R.string.title_text_tvshow);

            Call<TVShowDetail> call = apiService.TVShowDetail(getId, MainActivity.APIKEY, "en-US");
            call.enqueue(new Callback<TVShowDetail>() {
                @Override
                public void onResponse(Call<TVShowDetail> call, Response<TVShowDetail> response) {
                    String dataTitle = response.body().getTitle();
                    String dataVote = response.body().getVote_average();
                    String dataReleaseDate = response.body().getRelease_date();
                    String dataPopular = response.body().getPopularity();
                    String dataOverview = response.body().getOverview();
                    String dataPoster = response.body().getPoster_path();
                    String dataBackdrop = response.body().getBackdrop_path();
                    String[] arr = {dataTitle, dataVote, dataReleaseDate, dataPopular, dataOverview, dataPoster, dataBackdrop};
                    Collection<String> l = Arrays.asList(arr);
                    lists.addAll(l);
                    responDetail(lists);
                }

                @Override
                public void onFailure(Call<TVShowDetail> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.alertDetailLoadTVShow), Toast.LENGTH_LONG).show();
                }
            });
        } else {
            setActionBarTitle(R.string.title_text_movie);

            Call<MovieDetail> call = apiService.movieDetail(getId, MainActivity.APIKEY, "en-US");
            call.enqueue(new Callback<MovieDetail>() {
                @Override
                public void onResponse(Call<MovieDetail> call, Response<MovieDetail> response) {
                    String dataTitle = response.body().getTitle();
                    String dataVote = response.body().getVote_average();
                    String dataReleaseDate = response.body().getRelease_date();
                    String dataPopular = response.body().getPopularity();
                    String dataOverview = response.body().getOverview();
                    String dataPoster = response.body().getPoster_path();
                    String dataBackdrop = response.body().getBackdrop_path();
                    String[] arr = {dataTitle, dataVote, dataReleaseDate, dataPopular, dataOverview, dataPoster, dataBackdrop};
                    Collection<String> l = Arrays.asList(arr);
                    lists.addAll(l);
                    responDetail(lists);
                }

                @Override
                public void onFailure(Call<MovieDetail> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), getResources().getString(R.string.alertDetailLoadMovie), Toast.LENGTH_LONG).show();
                }
            });
        }
    }

    private void responDetail(ArrayList<String> lists) {
        showLoading(false);
        if (lists == null || lists.size() == 0) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.alertDetailLoadMovie), Toast.LENGTH_LONG).show();
        } else {
            detMovieName.setText(lists.get(0));
            detRating.setText(lists.get(1));

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            try {
                Date date1 = simpleDateFormat.parse(lists.get(2));
                Calendar cal = Calendar.getInstance();
                cal.setTime(date1);
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);
                int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);

                String dayMonth = "";
                if (day < 10) {
                    dayMonth = "0" + day;
                } else {
                    dayMonth = String.valueOf(day);
                }

                String cekDay = customDate.getDay(dayOfWeek);
                String dayWeek = cekDay.substring(0, 3);

                String cekMonth = customDate.getMonth(month);

                String tglRilis = dayWeek + ", " + dayMonth + " " + cekMonth + " " + year;

                detTglRilis.setText(tglRilis);
            } catch (ParseException e) {
                e.printStackTrace();
            }

            detPopularity.setText(lists.get(3));
            detMovieDesc.setText(lists.get(4));

            String poster = "https://image.tmdb.org/t/p/w154" + lists.get(5);
            String poster2 = "https://image.tmdb.org/t/p/w154" + lists.get(6);
            final String[] mImageId = {poster, poster2};
            gallery.setSpacing(1);
            GalleryImageAdapter galleryImageAdapter = new GalleryImageAdapter(getApplicationContext(), mImageId);
            gallery.setAdapter(galleryImageAdapter);
            Glide.with(getApplicationContext())
                    .load(poster)
                    .into(imageView);
            gallery.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Glide.with(getApplicationContext())
                            .load(mImageId[position])
                            .into(imageView);
                }
            });
        }
    }

    private void setActionBarTitle(int title_text_movie) {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(title_text_movie);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_change_settings) {
            Intent mIntent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
            startActivity(mIntent);
        } else if (item.getItemId() == R.id.action_reminder_settings) {
            Intent mIntent = new Intent(this, ReminderSettingsActivity.class);
            startActivity(mIntent);
        } else if (item.getItemId() == R.id.action_favorite) {
            AddFavorite(title_frag,getId,lists);
        } else if (item.getItemId() == R.id.action_favorite_saved) {
            DeleteFavorite(getId);
        } else if (item.getItemId() == R.id.action_menu_favorite) {
            Intent mIntent = new Intent(this,FavoriteActivity.class);
            startActivity(mIntent);
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    private void DeleteFavorite(Integer getId) {
        String id_movies = getId.toString();
        int result = favoriteHelper.deleteById(id_movies);
        if (result > 0) {
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.alert_delete_favo),Toast.LENGTH_LONG).show();
            catg_favo = null;
            invalidateOptionsMenu();
            Intent intent = new Intent(this, FavoriteBannerWidget.class);
            intent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        } else {
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.alert_fail_delete_favo),Toast.LENGTH_LONG).show();
        }
    }

    private void AddFavorite(String title_frag, Integer getId, ArrayList<String> lists){
        ContentValues values = new ContentValues();
        values.put(DatabaseContract.FavoriteColumn.ID_MOVIES,getId);
        values.put(DatabaseContract.FavoriteColumn.TYPE,title_frag);
        values.put(DatabaseContract.FavoriteColumn.TITLE,lists.get(0));
        values.put(DatabaseContract.FavoriteColumn.RATING,lists.get(1));
        values.put(DatabaseContract.FavoriteColumn.RELEASE_DATE,lists.get(2));
        values.put(DatabaseContract.FavoriteColumn.POPULARITY,lists.get(3));
        values.put(DatabaseContract.FavoriteColumn.OVERVIEW,lists.get(4));
        values.put(DatabaseContract.FavoriteColumn.POSTER,lists.get(5));
        values.put(DatabaseContract.FavoriteColumn.BACKDROP,lists.get(6));
        long result = favoriteHelper.insert(values);
        if (result > 0) {
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.alert_add_favo),Toast.LENGTH_LONG).show();
            catg_favo = "Movie";
            invalidateOptionsMenu();
        }
        else {
            Toast.makeText(getApplicationContext(),getResources().getString(R.string.alert_fail_add_favo),Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (catg_favo == null) {
            MenuItem menuItem = menu.findItem(R.id.action_favorite_saved);
            menuItem.setVisible(false);
            MenuItem menuItem2 = menu.findItem(R.id.action_favorite);
            menuItem2.setVisible(true);
        } else {
            MenuItem menuItem = menu.findItem(R.id.action_favorite_saved);
            menuItem.setVisible(true);
            MenuItem menuItem2 = menu.findItem(R.id.action_favorite);
            menuItem2.setVisible(false);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    private void showLoading(Boolean state) {
        if (state) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressBar.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        if (catg_favo != null) {
            outState.putString(CATG_FAVO, catg_favo);
        }
        outState.putString(TITLE_DETAIL, title_frag);
        outState.putInt(ID_DETAIL, getId);
        outState.putStringArrayList(SOME_VALUE_KEY, lists);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        favoriteHelper.close();
    }

    @Override
    public void preExecute() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                showLoading(true);
            }
        });
    }

    @Override
    public void postExecute(ArrayList<MovieFavo> favos, Integer getId) {
        showLoading(false);
        if (favos.size() > 0) {
            String dataTitle = favos.get(0).getTitle();
            String dataVote = favos.get(0).getRating();
            String dataReleaseDate = favos.get(0).getRelase_date();
            String dataPopular = favos.get(0).getPopularity();
            String dataOverview = favos.get(0).getOverview();
            String dataPoster = favos.get(0).getPoster();
            String dataBackdrop = favos.get(0).getBackdrop();
            String[] arr = {dataTitle, dataVote, dataReleaseDate, dataPopular, dataOverview, dataPoster, dataBackdrop};
            Collection<String> l = Arrays.asList(arr);
            lists.addAll(l);
            responDetail(lists);
            catg_favo = "Movie";
            invalidateOptionsMenu();
        } else {
            if (catg_favo == null) {
                Connection(getId);
            } else {
                Toast.makeText(this, "Load Detail Failed", Toast.LENGTH_LONG).show();
            }
        }
    }

    private static class LoadFavoMovieAsync extends AsyncTask<Void, Void, ArrayList<MovieFavo>> {

        private final WeakReference<FavoriteHelper> weakFavoHelper;
        private final WeakReference<LoadFavoCallback> weakCallback;
        private final String weakIdMovie;
        private final Integer IdMovie;

        private LoadFavoMovieAsync(FavoriteHelper favoriteHelper, LoadFavoCallback callback, String idMovie) {
            weakFavoHelper = new WeakReference<>(favoriteHelper);
            weakCallback = new WeakReference<>(callback);
            weakIdMovie = idMovie;
            IdMovie = Integer.valueOf(idMovie);
        }

        @Override
        protected ArrayList<MovieFavo> doInBackground(Void... voids) {
            Cursor dataCursor = weakFavoHelper.get().queryByIdMovies(weakIdMovie);
            return MappingHelper.mapCursorToArrayList(dataCursor);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            weakCallback.get().preExecute();
        }

        @Override
        protected void onPostExecute(ArrayList<MovieFavo> favos) {
            super.onPostExecute(favos);
            weakCallback.get().postExecute(favos,IdMovie);
        }
    }

    private void link() {

        favoriteHelper = FavoriteHelper.getInstance(getApplicationContext());
        favoriteHelper.open();

        customDate = new CustomDate();

        detMovieName = findViewById(R.id.tv_det_movie_name);
        detRating = findViewById(R.id.tv_det_rating);
        detTglRilis = findViewById(R.id.tv_det_tglrilis);
        detPopularity = findViewById(R.id.tv_det_popular);
        detMovieDesc = findViewById(R.id.tv_det_movie_desc);
        gallery = findViewById(R.id.gallery_movie);
        imageView = findViewById(R.id.img_view_movie);
        progressBar = findViewById(R.id.progressBar);
    }
}

interface LoadFavoCallback {
    void preExecute();
    void postExecute(ArrayList<MovieFavo> favos,Integer getId);
}